import zookeeper
import time
import redis
import os
import json
import smtplib
from email.mime.text import MIMEText
from email.header import Header

class CodisSupervisor(object):
    
    CONNECTION_STAT = (0,3,4,-112)
    
    CONNECTION_FAIL = "Can't connect to zookeeper server."
    CODIS_NOT_RUNNING = "Codis is probably not running."
    CANT_FIND_CODIS_INSTANCE ="Can't find your codis instance."
    
    CODIS_ZK_NODE = '/zk/codis'
    CODIS_SERVER_STATE_KEY = 'codis_server_state'
    
    def __init__(self,codis_path,config_path,zk_server = '127.0.0.1:2181',product = None):
        
        self.zk_server = zk_server
        self.product = product
        self.codis_path = codis_path
        self.config_path = config_path
        self.is_ok = True
        try:
            self.zk_handle = zookeeper.init(zk_server)
            conn_state = zookeeper.state(self.zk_handle)
            while not conn_state in self.CONNECTION_STAT:
                time.sleep(1)
                conn_state = zookeeper.state(self.zk_handle)
            if conn_state != 3:
                self.is_ok = False
                print self.CONNECTION_FAIL
            else:
                codis_state = self.check_codis_instance()
                if codis_state != 'ok':
                    self.is_ok = False
                    print codis_state                
        except:
            self.is_ok = False
            print self.CONNECTION_FAIL
    
    def check_codis_instance(self):        
        children = zookeeper.get_children(self.zk_handle,'/')
        if 'zk' in children:
            children = zookeeper.get_children(self.zk_handle,'/zk')
            if 'codis' in children:
                if not self.product:
                    return 'ok'
                else:
                    instance = 'db_' + self.product
                    children = zookeeper.get_children(self.zk_handle,'/zk/codis')
                    if instance in children:
                        return 'ok'
                    else:
                        return self.CANT_FIND_CODIS_INSTANCE
            else:
                return self.CODIS_NOT_RUNNING
        else:
            return self.CODIS_NOT_RUNNING
    
    def get_server_info(self,product = None):
        if not product:
            product = self.product
        server_info = {}
        node = self.CODIS_ZK_NODE + '/' + 'db_' + product + '/servers'
        group_list = zookeeper.get_children(self.zk_handle,node)
        for group in group_list:
            group_node = node + '/' + group
            redis_list = zookeeper.get_children(self.zk_handle,group_node)
            server_info[group] = {}
            server_info[group]['slave'] = []
            for redis_host in redis_list:
                redis_node = group_node + '/' + redis_host
                redis_info = json.loads(zookeeper.get(self.zk_handle,redis_node)[0])
                if redis_info['type'] == 'master':
                    server_info[group]['master'] = redis_host
                elif redis_info['type'] == 'slave':
                    server_info[group]['slave'].append(redis_host)
        return server_info
    
    def check_codis_server(self,addr):
        try:
            host,port = addr.split(':')
            r = redis.Redis(host,int(port))
            r.get(self.CODIS_SERVER_STATE_KEY)
        except:
            return False
        return True
    
    def promote(self,group_id,group):
        error = False
        _id = int(group_id.split('_',1)[1])
        while len(group['slave']) >0:
            addr = group['slave'].pop(0)
            if self.check_codis_server(addr):
                try:
                    os.system('%s -c %s server promote %s %s'%(self.codis_path,self.config_path,_id,addr))
                    return {'_id':group_id,'result': 'Promote success'}
                except:
                    error = True
        if error:
            return {'_id':group_id,'result':'Unexcepted error' , 'error_info':error_info}
        else:
            return {'_id':group_id,'result':'No slave to promote'}
    
    def check(self): 
        result = []
        codis_group = self.get_server_info()    
        for group_id,group in codis_group.iteritems():
            if not self.check_codis_server(group['master']):
                result.append(json.dumps(self.promote(group_id,codis_group[group_id])))
        return result

if __name__ == "__main__":
    go_path = os.getenv('GOPATH')
    codis_path = go_path +'/src/bitbucket.org/gt-dev/gt-codis'
    supervisor = CodisSupervisor(codis_path +'/bin/codis-config',codis_path + '/sample/config.ini','127.0.0.1:2181','test')
    result = supervisor.check()
    if result:
        mail_text = '\r\n'.join(result)
        msg = MIMEText(mail_text,"plain","utf-8")
        msg['Subject'] = Header('redis_down','utf-8')
        smtp = smtplib.SMTP()
        smtp.connect("smtp.qq.com")
        smtp.login("","")
        smtp.sendmail("","",msg.as_string())
        smtp.quit()